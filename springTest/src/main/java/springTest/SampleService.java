package springTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("applicationContext.xml")
public class SampleService {
	
	
	public static void main(String[] args) {
		
		
		ConfigurableApplicationContext appContext=SpringApplication.run(SampleService.class, args);
		
		for(String name:appContext.getBeanDefinitionNames()){
			
			System.out.println(name);
			
		}
		
	}

}
